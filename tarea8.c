#include <stdio.h>
int main(int argc, char const *argv[]) {
  printf("Valor en bytes de las siguientes variables:\n" );
  printf("Variable int ocupa %lu bytes\n",sizeof(int) );
  printf("Variable float ocupa %lu bytes\n",sizeof(float) );
  printf("Variable char ocupa %lu bytes\n",sizeof(char) );
  printf("Variable long int ocupa %lu bytes\n",sizeof(long int) );
  return 0;
}
