struct auto {
  int llantas=4;
  int puertas=4;
  int pasajeros=4;
  int motor=1;
  char nombreConductor[30];
  int vidrios=6;
};
struct computadora{
  int procesador=1;
  int tarjetaMadre=1;
  char modelo[10];
  int teclado=1;
  int bocinas=2;
  int pantalla=1;
  int fuentePoder=1;
};
struct alumno{
  char nombre [20];
  int edad=18;
  float estatura=1.70;
  char genero;
  char carrera[20];
};
struct equipoFutbol{
  char nombre[20];
  int jugadores=28;
  int titulares=11;
  int delanteros=5
  int medios=9;
  int defensas=10;
  int porteros=4;
  char nombreJugador=[30];
};
struct mascota{
  char nombre[10];
  char especie[10];
  char genero;
  int patas;
  int cola;
  int edad;
  char alimento[20];
};
struct videojuego{
  char plataforma[10];
  char genero[10];
  int jugadores=1;
  int horasJugables=8;
  char objetivos[30];
  int personajes;
  int año;
};
struct Superheroe {
  char nombre[20];
  char habilidades[50];
  char identidadSecreta[30];
  int aliados;
  int edad;
  char ciudad;
};
struct planetas{
  int tamaño;
  float distanciaAlSol;
  char nombre[20];
  float diametro;
  int lunas;
  float temperatura;
};
struct personajeJuegoFavLeonSKennedy{
  int edad;
  float estatura;
  char ocupacion[30];
  char armas[50];
  int objetivoscumplidos;
  char enemigos[40];
  int enemigosderrotados;
}
struct comic {
  int paginas;
  int cuadros;
  char nombre[20];
  char personajePrincipal[30];
  int añoPublicacion;
  char autor[30];
  char sipnosis[100];
}
