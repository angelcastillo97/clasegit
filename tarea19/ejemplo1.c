#include <stdio.h>
#include <stdlib.h>




int main(int argc, char const *argv[]) {
printf("Tipos de datos y direcciones de memoria\n"
"19/10/2014\n");

char letra='a';
unsigned char letraExtendida='a';

short enteroCorto=10;
int entero=30123;
unsigned int enteroLargo=2345898898;

float decimal=11.4f;
double decimalDoblePrec=12.9989;

printf("Valor: %c direccion: %x, tamano: %d Byte(s)\n",letra,&letra,sizeof(letra));
printf("Valor: %c direccion: %x, tamano: %d Byte(s)\n",letraExtendida,&letraExtendida,sizeof(letraExtendida));

system("pause");
  return 0;
}
